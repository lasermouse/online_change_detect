# Automatic Differentiation using jax
# https://github.com/google/jax
import jax.numpy as np
import numpy as onp  # original numpy for indexed assignment/mutation (outside context of differentiation)
import numpy.random as npr # randomisation for minibatch gradient descent
from jax import grad, jit, vmap
from jax.experimental import optimizers
import jax.random as random
import jax.lax as lax

from jax import device_put  # onp operations not transferred to GPU, so they should run faster

# Other things
import hmm  # functions to simulate data
import matplotlib.pyplot as plt
import scipy.io
from tqdm import tqdm
import itertools

import os
import pickle as pkl
import pandas as pd
from os.path import expanduser


from scipy.signal import savgol_filter

# debugging nans returned by grad
from jax.config import config

# Cross validation
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

# Plotting functions
import normative_plot as nmt_plot
import functools  # to pass around sigmoid functions for plotting

def cal_p_x_given_z(x_k):
    z_mu = np.log(np.array([1.0, 1.25, 1.35, 1.50, 2.00, 4.00]))
    z_var = np.array([0.25, 0.25, 0.25, 0.25, 0.25, 0.25]) ** 2 # in actual data, std is 0.25
    # TODO: Variance needs to be converted from base 2 form back to base e form

    # z_mu = np.log(np.array([1.0, 1.25]))
    # z_var = np.array([0.25, 0.25])

    p_x_given_z = (1 / np.sqrt(2 * np.pi * z_var)) * np.exp(-(x_k - z_mu) ** 2 / (2 * z_var))

    # returns row vector (for simpler calculation later on)
    return p_x_given_z.T


def forward_inference(x):
    # rewritten to use without assignment of the form: A[i, j] = x
    # TODO: Think about logging thing to prevent underflow
    p_z1_given_x = list()
    p_z2_given_x = list()

    hazard_rate = 0.0001

    transition_matrix = np.array([[1 - hazard_rate, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0],
                                  [0, 1, 0, 0, 0, 0],
                                  [0, 0, 1, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 0],
                                  [0, 0, 0, 0, 1, 0],
                                  [0, 0, 0, 0, 0, 1]])
    init_state_probability = np.array([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    # transition_matrix = np.array([[1 - hazard_rate, hazard_rate],
    #                               [0, 1]])

    # List to store the posterior: p(z_k | x_{1:k}) for k = 1, ..., n
    p_z_given_x = np.zeros((len(x), 6)) # this will be a n x M matrix, not sure if this is can be created without array assignment..
    p_change_given_x = list()
    p_baseline_given_x = list()
    p_xk_given_z_store = np.zeros((len(x), 6))

    # Initial probabilities
    p_z_and_x = cal_p_x_given_z(x_k=x[0]) * init_state_probability

    # add the initial probability to the output list
    p_change_given_x.append(0)


    # Loop through the rest of the samples to compute P(x_k, z_k) for each
    for k in np.arange(1, len(x)):
        p_xk_given_z = cal_p_x_given_z(x_k=x[k])


        # update joint
        p_z_and_x = np.dot((p_xk_given_z * p_z_and_x), transition_matrix)

        # NOTE: This step is not the conventional forward algorithm, but works.
        # p_z_given_x[k, :] = p_z_and_x / np.sum(p_z_and_x)
        # p_zk_given_xk = p_z_and_x / np.sum(p_z_and_x)
        p_zk_given_xk = np.divide(p_z_and_x, np.sum(p_z_and_x))

        p_change_given_x.append(np.sum(p_zk_given_xk[1:])) # sum from the second element
        # p_baseline_given_x.append(p_z_given_x[0]) # Just 1 - p_change

    # return p_z_given_x
    return np.array(p_change_given_x)
    # return p_xk_given_z_store


def forward_inference_custom_transition_matrix(x):
    """
    :param transtiion_matrix_list: global variable with list of transition matrices
    :param x: signal to predict
    :return:
    """

    p_z1_given_x = list()
    p_z2_given_x = list()

    init_state_probability = np.array([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    # List to store the posterior: p(z_k | x_{1:k}) for k = 1, ..., n
    p_z_given_x = np.zeros((len(x), 6)) # this will be a n x M matrix,
    # not sure if this is can be created without array assignment..
    p_change_given_x = list()
    p_baseline_given_x = list()
    # p_xk_given_z_store = np.zeros((len(x), 6))

    # Initial probabilities
    p_z_and_x = cal_p_x_given_z(x_k=x[0]) * init_state_probability
    p_z_given_x = p_z_and_x / np.sum(p_z_and_x)


    # add the initial probability to the output list
    p_change_given_x.append(0)

    # Loop through the rest of the samples to compute P(x_k, z_k) for each
    for k in np.arange(1, len(x)):
        p_xk_given_z = cal_p_x_given_z(x_k=x[k])

        # update conditional probability
        p_z_and_x = np.dot((p_xk_given_z * p_z_given_x), transition_matrix_list[k-1])

        # NOTE: This step is not the conventional forward algorithm, but works.
        # p_z_given_x[k, :] = p_z_and_x / np.sum(p_z_and_x)
        # p_zk_given_xk = p_z_and_x / np.sum(p_z_and_x)
        p_z_given_x = np.divide(p_z_and_x, np.sum(p_z_and_x))

        p_change_given_x.append(np.sum(p_z_given_x[1:])) # sum from the second element
        # p_baseline_given_x.append(p_z_given_x[0]) # Just 1 - p_change

    return np.array(p_change_given_x)


def apply_cost_benefit(change_posterior, true_positive=1.0, true_negative=1.0, false_negative=1.0, false_positive=1.0):
    """
    computes decision value based on cost and benefit
    note that true-positive is always set to 1.0, everything else is relative to it.
    some restrictions on parameter space:
         - (soft restriction) each weight value must be greater than 0
         - (hard restriction) sum of all weights must be greater than 0
    :param true_positive: relative reward for getting a Hit (lick when there is a change)
    :param true_negative: relative reward for a correct rejection (not licking when there is no change)
    :return:
    """
    # ensure parameter constraints are met
    small_value = 1e-9
    true_negative = np.max([small_value, true_negative])
    false_negative = np.max([small_value, false_negative])
    false_positive = np.max([small_value, false_positive])

    # cost benefit calculation
    lick_benefit = change_posterior * true_positive
    lick_cost = (1 - change_posterior) * false_positive
    no_lick_benefit = (1 - change_posterior) * true_negative
    no_lick_cost = change_posterior * false_negative

    lick_value = lick_benefit - lick_cost
    no_lick_value = no_lick_benefit - no_lick_cost

    prob_lick = (lick_value - no_lick_value) / (true_positive + true_negative + false_negative + false_positive)

    return prob_lick


def apply_strategy(prob_lick, k=10, midpoint=0.5, max_val=1.0, min_val=0.0,
                   policy="sigmoid", epsilon=0.1, lick_bias=0.5):
    # four param logistic function
    # prob_lick = y = a * 1 / (1 + np.exp(-k * (x - x0))) + b

    if policy == "sigmoid":
        # two param logistic function
        p_lick = (max_val - min_val) / (1 + np.exp(-k * (prob_lick - midpoint))) + min_val
    elif policy == "epsilon-greedy":
        # epsilon-greedy policy
        p_lick = prob_lick * (1 - epsilon) + epsilon * lick_bias

    return p_lick


def apply_softmax_decision_rule(lick_value, no_lick_value, beta):
    """
    Applies softmax decision rule
    :param lick_value:
    :param no_lick_value:
    :param beta: inverse temperature (sometimes denoted as gamma in the literature)
    :return:
    """
    p_lick = 1 / (1 + np.exp(-beta * (lick_value - no_lick_value)))
    return p_lick


def test_on_data(exp_data_file, change_val=1.0):

    with open(exp_data_file, "rb") as handle:
        exp_data = pkl.load(handle)

    trial_type_list = exp_data["hazard"]
    change_magnitude = np.exp(exp_data["sig"].flatten())
    # noiseless_trial_type = exp_data["noiseless"].flatten()
    # mouse_abort = (exp_data["outcome"].flatten() == "abort").astype(float)

    # remove aborted trials, and noisless trials
    if change_magnitude is not None:
        # trial_index = np.where((mouse_abort == 0) & (noiseless_trial_type == 0) * (change_magnitude == change_val))[0]
        trial_index = onp.where(change_magnitude == change_val)[0]
    else:
        trial_index = onp.where((mouse_abort == 0) & (noiseless_trial_type == 0))[0]

    signal = exp_data["ys"].flatten()[trial_index][0][0]
    tau = exp_data["change"][trial_index][0][0]

    # use custom transition matrix
    global transition_matrix_list
    hazard_rate, transition_matrix_list = get_hazard_rate(
        hazard_rate_type="subjective", datapath=exp_data_file
    )
    p_z_given_x = forward_inference_custom_transition_matrix(signal)

    global num_non_hazard_rate_params
    num_non_hazard_rate_params = 3

    param_vals = np.array([10.0, 0.0, 0.1, 0.0, 0.0, 0.0])
    param_vals = np.hstack([param_vals, hazard_rate])
    p_lick = predict_lick_w_hazard_rate(param_vals=param_vals, signal=signal)

    # Plot example
    plot_signal_and_inference(signal=signal, tau=tau, prob=p_z_given_x)


def posterior_to_decision(posterior, return_prob=True, k=1.0, false_negative=0.0, midpoint=0.5):
    change_posterior = posterior  # p(z_2 | x_{1:k})
    p_lick = apply_cost_benefit(
        change_posterior, true_positive=1.0, true_negative=0.0,
        false_negative=false_negative, false_positive=0.0
    )
    p_lick = apply_strategy(p_lick, k=k, midpoint=midpoint)

    if return_prob is True:
        return p_lick


def create_vectorised_data(exp_data_path, subset_trials=None, lick_exponential_decay=False, decay_constant=0.5,
                           full_trial_signal=False):
    """
    Create a matrix from signal and lick vectors of unequal length so that computations can be vectorised.
    :param exp_data_path:
    :param subset_trials:
    :param lick_exponential_decay: If true, then lick vector will be an exponential decay function starting
    at the point of lick and decaying backwards in time. This may help the model fit better.
    :return:
    """
    # LOADING DATA
    with open(exp_data_path, "rb") as handle:
        exp_data = pkl.load(handle)

    mouse_rt = exp_data["rt"].flatten()
    signal = exp_data["ys"].flatten()
    change_magnitude = exp_data["sig"].flatten()

    # CONVERTING VECTORS TO A PADDED MATRIX SO THINGS CAN BE VECTORISED
    # reshape signal to a matrix (so we can do vectorised operations on it)
    num_time_samples = list()
    for s in signal:
        num_time_samples.append(len(s[0]))
    max_time_bin = onp.max(num_time_samples)
    num_trial = len(signal)

    signal_matrix = onp.zeros(shape=(num_trial, max_time_bin))

    for n, s in enumerate(signal):
        signal_matrix[n, :len(s[0])] = s[0] # up to full length of the signal in the trial
        signal_matrix[n, len(s[0]):] = onp.random.normal(loc=change_magnitude[n], scale=0.0625,
                                                         size=(max_time_bin - len(s[0])))

    lick_matrix = onp.zeros(shape=(num_trial, max_time_bin))
    lick_matrix[:] = 99

    # fill the matrix with ones and zeros
    for trial in np.arange(0, len(mouse_rt)):
        if not onp.isnan(mouse_rt[trial]):
            mouse_lick_vector = onp.zeros(shape=(int(mouse_rt[trial]), ))
            mouse_lick_vector[int(mouse_rt[trial] - 1)] = 1 # note the zero-indexing
            if lick_exponential_decay is True:
                # TODO: Plot this in a separate block, there is wasted computational time upstairs.
                time_before_lick = np.arange(0, int(mouse_rt[trial] - 1))
                exp_lick = np.exp(-decay_constant * time_before_lick)
                mouse_lick_vector = np.flipud(exp_lick)
        else:
            mouse_lick_vector = onp.zeros(shape=(len(signal[trial][0])), )
        lick_matrix[trial, :len(mouse_lick_vector)] = mouse_lick_vector

    return signal_matrix, lick_matrix


def predict_lick(param_vals, signal):
    # NOTE: This function also depends on the global parameter "time_shift"

    # multiply standard normal by constant: aX + b = N(au + b, a^2\sigma^2)
    posterior = forward_inference_custom_transition_matrix(signal)

    p_lick = apply_cost_benefit(
        change_posterior=posterior, true_positive=1.0, true_negative=0.0,
        false_negative=0.0, false_positive=0.0
    ) # param_vals[1]
    p_lick = apply_strategy(
        p_lick, k=nonstandard_sigmoid(param_vals[0], min_val=0, max_val=20),
        midpoint=nonstandard_sigmoid(param_vals[1], min_val=-10, max_val=10),
        max_val=1-1e-5, min_val=1e-5
    )
    # Add time shift
    if time_shift > 0:
        baseline = 0.88888888 # nan placeholder
        p_lick = np.concatenate([np.repeat(baseline, time_shift), p_lick])
        p_lick = p_lick[0:len(signal.flatten())]  # clip p(lick) to be same length as actual lick
    elif time_shift < 0:
        # backward shift
        baseline = 0.88888888  # nan placeholder
        p_lick = np.concatenate([p_lick, np.repeat(baseline, abs(time_shift))])
        p_lick = p_lick[abs(time_shift):(len(signal.flatten()) + abs(time_shift))]
    return p_lick


def loss_function_batch(param_vals, batch):
    # missing arguments: signals, param_vals_, actual_lick_,
    """
    loss between predicted licks and actual licks
    computed using cross entropy.
    This version computes the loss for a batch.

    Note that loss is now always a positive value (this seems to be the assumed input of JAX optimizers, in contrast to
    autograd optimisers, which seems to take negative loss values).

    """
    signal, lick = batch

    batch_predictions = batched_predict_lick(param_vals, signal)
    # batch_loss = batched_cross_entropy(actual_lick_vector=lick_matrix, p_lick=batch_predictions)
    # batch_loss = matrix_cross_entropy_loss(lick, batch_predictions)
    batch_loss = matrix_weighted_cross_entropy_loss(lick, batch_predictions, alpha=1, cumulative_lick=False)

    # adding log barrier
    # c = 10
    # barrier_loss = - c * np.sum(np.log(param_vals[num_non_hazard_rate_params:])) - c * np.sum(np.log(1 - param_vals[num_non_hazard_rate_params:]))
    # batch_loss = batch_loss + barrier_loss
    # print("Barrier loss: ", str(barrier_loss))

    return batch_loss


def performance_batch(param_vals, batch):
    """
    Same as loss_function_batch, but also calculates other model peformance measures (accuracy, recall)
    :param param_vals:
    :param batch:
    :return:
    """
    signal, lick = batch
    batch_predictions = batched_predict_lick(param_vals, signal)
    batch_loss = matrix_cross_entropy_loss(lick, batch_predictions)

    # TODO: overall accuracy (regardless of time)
    # This may actually require sampling from the model...



def cross_entropy_loss(actual_lick_vector, p_lick):
    # find last actual value of the lick vector (effectively like a mask)
    # Can't use single argument np.where in JAX
    # first_invalid_value = np.where(actual_lick_vector == 99, 1, 0)
    # actual_lick_vector = actual_lick_vector[:first_invalid_value]
    # p_lick = p_lick[:first_invalid_value]

    # Can't use boolean indexing in JAX
    # actual_lick = actual_lick_vector[actual_lick_vector != 99]
    # p_lick = p_lick[actual_lick_vector != 99]

    # dot product indexing that uses 3 argument np.where
    # index_values = np.where(actual_lick_vector)

    # actual_lick = actual_lick_vector

    cross_entropy = -(np.dot(actual_lick_vector, np.log(p_lick)) + np.dot((1 - actual_lick_vector), np.log(1-p_lick)))
    return cross_entropy

VALID_VALUE = 0.88888888

def matrix_cross_entropy_loss(lick_matrix, prediction_matrix):
    mask = np.where(lick_matrix == 99, 0, 1)
    prediction_matrix_mask = np.where(prediction_matrix == 0.88888888, 0, 1)
    cross_entropy = - ((lick_matrix * np.log(prediction_matrix)) + (1 - lick_matrix) * np.log(1-prediction_matrix))
    cross_entropy = cross_entropy * mask * prediction_matrix_mask

    return np.nansum(cross_entropy)  # nansum is just a quick fix, likely need to be more principled...


def matrix_weighted_cross_entropy_loss(lick_matrix, prediction_matrix, alpha=1, cumulative_lick=False):
    mask = np.where(lick_matrix == 99, 0, 1)
    prediction_matrix_mask = np.where(prediction_matrix == 0.88888888, 0, 1)
    ################## Cross entropy on the cumulative p(lick) ###########################
    if cumulative_lick is True:
        small_value = 1e-5 # can't be zero because then the cross entropy will have to do log(0)
        prediction_matrix = np.where(prediction_matrix == 0.88888888, small_value, prediction_matrix) # time shift p(lick) = 0
        prediction_matrix = batched_cumulative_lick(None, prediction_matrix)
        prediction_matrix = np.where(prediction_matrix >=1, 1-small_value, prediction_matrix)
        prediction_matrix = np.where(prediction_matrix <=0, small_value, prediction_matrix)

    ################# Cross Entropy on the Instataneous p(lick) #######################
    # modifying cross entropy to prevent NaNs (probably due to underflow of prediction_matrix?)
    # small_value = 0 # originally 1e-9
    # cross_entropy = - (alpha * lick_matrix * np.log(np.maximum(prediction_matrix, small_value)) +
    #                   (1 - lick_matrix) * np.log(1 - np.maximum(prediction_matrix, small_value))
    #                   )

    # hard threshold prediction matrix
    small_value = 1e-5
    prediction_matrix = np.where(prediction_matrix >= 1, 1 - small_value, prediction_matrix)
    prediction_matrix = np.where(prediction_matrix <= 0, small_value, prediction_matrix)

    cross_entropy = - (alpha * lick_matrix * np.log(prediction_matrix) +
                       (1 - lick_matrix) * np.log(1 - prediction_matrix)
                       )

    cross_entropy = cross_entropy * mask * prediction_matrix_mask

    return np.sum(cross_entropy)


def run_through_dataset_fit_vector(datapath, savepath, training_savepath, param=None, num_non_hazard_rate_param=2,
                                   fit_hazard_rate=True,
                                   cv=False,
                                   epoch_index=None):
    """
    Takes in the experimental data, and makes inference of p(lick) for each time point given the stimuli
    :param datapath:
    :param savepath:
    :param param:
    :return:
    """
    if param is None:
        # if no parameters specified, then load the training result and get the last param
        with open(training_savepath, "rb") as handle:
            training_result = pkl.load(handle)

    if epoch_index is None and cv is False:
        min_loss_epoch_index = onp.where(training_result["loss"] == min(training_result["loss"]))[0][0]
        epoch_index = min_loss_epoch_index
    elif epoch_index is None and cv is True:
        min_val_loss_epoch_index = onp.where(training_result["val_loss"] == min(training_result["val_loss"]))[0][0]
        epoch_index = min_val_loss_epoch_index
    # normally -1 (last training epoch)
    if cv is False:
        param = training_result["param_val"][epoch_index]
        print("Parameter training loss: ",  str(training_result["loss"][epoch_index])) # just to double check
    else:
        param = training_result["param_val"][epoch_index]
        print("Parameter training loss: ", str(training_result["val_loss"][epoch_index]))
    # note that parameters do not need to be pre-processed
    global time_shift
    time_shift = 0

    with open(datapath, "rb") as handle:
        exp_data = pkl.load(handle)

    signals = exp_data["ys"].flatten()
    change = np.exp(exp_data["sig"].flatten())
    tau = exp_data["change"].flatten()
    absolute_decision_time = exp_data["rt"].flatten()
    # Find out when the trial actually ends, and create a mask of that:
    # (1 means trial still happening, 0 means those are padded signals)
    # (onp is used here to allow assignment into array, which is valid because this procedure is not in gradient descent
    trial_duration_list = list()
    for s in signals:
        trial_duration_list.append(len(s[0]))

    num_trial = len(signals)
    max_duration = onp.max(trial_duration_list)

    trial_duration = onp.array(trial_duration_list) - 1 # convert to 0 indexing
    trial_duration_mask = onp.zeros(shape=(num_trial, max_duration))

    for n, trial_duration in enumerate(trial_duration_list):
        trial_duration_mask[n, :trial_duration] = 1

    signal_matrix, lick_matrix = create_vectorised_data(datapath)

    global num_non_hazard_rate_params
    num_non_hazard_rate_params = num_non_hazard_rate_param # this is for predict_lick_w_hazard_rate

    if fit_hazard_rate is True:
        batched_predict_lick = vmap(predict_lick_w_hazard_rate, in_axes=(None, 0))
    else:
        global transition_matrix_list
        _, transition_matrix_list = get_hazard_rate(hazard_rate_type="constant", constant_val=0.0001, datapath=datapath)
        batched_predict_lick = vmap(predict_lick, in_axes=(None, 0))
    # signals has to be specially prepared to get the vectorised code running
    prediction_matrix = batched_predict_lick(param, signal_matrix)
    # Prediction matrix truncated by the lick time of the mouse
    prediction_matrix = np.where(lick_matrix == 99, onp.nan, prediction_matrix)

    batched_cumulative_lick = vmap(predict_cumulative_lick, in_axes=(None, 0))
    if time_shift == 0:
        # if there is time shift, then can't use the initial padding values
        # batched_cumulative_lick = vmap(predict_cumulative_lick, in_axes=(0, None))
        cumulative_lick_matrix = batched_cumulative_lick(None, prediction_matrix)

    # full signal matrix
    full_signal_prediction_matrix = batched_predict_lick(param, signal_matrix)
    full_signal_prediction_matrix = np.where(trial_duration_mask == 0, onp.nan, full_signal_prediction_matrix)

    if time_shift != 0:
        # convert baseline to something sensible
        baseline_p_lick = 0.0001
        prediction_matrix = np.where(prediction_matrix == 0.88888888, baseline_p_lick, prediction_matrix)
        full_signal_prediction_matrix = np.where(full_signal_prediction_matrix == 0.88888888, baseline_p_lick,
                                                 full_signal_prediction_matrix)

        cumulative_lick_matrix = batched_cumulative_lick(None, prediction_matrix)


    vec_dict = dict()
    vec_dict["change_value"] = change
    vec_dict["rt"] = absolute_decision_time
    vec_dict["model_vec_output"] = prediction_matrix
    vec_dict["model_vec_output_cumulative"] = cumulative_lick_matrix
    vec_dict["full_signal_model_vec_output"] = full_signal_prediction_matrix
    vec_dict["true_change_time"] = tau
    vec_dict["epoch_index"] = epoch_index

    with open(savepath, "wb") as handle:
        pkl.dump(vec_dict, handle)


def plot_training_result(training_savepath):
    with open(training_savepath, "rb") as handle:
        training_result = pkl.load(handle)

    loss = np.concatenate(training_result["loss"])
    param_val = np.concatenate(training_result["param_val"])

    fig, axs = plt.subplots(2, 1, figsize=(8, 8))

    axs[0].plot(np.arange(1, len(loss) + 1), -loss)
    axs[1].plot(np.arange(1, len(loss) + 1), param_val)

    axs[0].set_ylabel("Loss")
    axs[1].set_ylabel("Parameter value")

    plt.show()


def compare_model_with_behaviour(model_behaviour_df_path, savepath=None, showfig=True):
    model_behaviour_df = pd.read_pickle(model_behaviour_df_path)
    # plot psychometric curve
    model_prop_choice = model_behaviour_df.groupby(["change"], as_index=False).agg({"decision": "mean"})
    mouse_prop_choice = model_behaviour_df.groupby(["change"], as_index=False).agg({"mouse_lick": "mean"})

    fig, ax = plt.subplots(figsize=(8, 6))
    # Plot model behaviour
    ax.plot(model_prop_choice.change, model_prop_choice.decision)
    ax.scatter(model_prop_choice.change, model_prop_choice.decision)
    # Plot mouse behaviour
    ax.plot(mouse_prop_choice.change, mouse_prop_choice.mouse_lick)
    ax.scatter(mouse_prop_choice.change, mouse_prop_choice.mouse_lick)

    ax.legend(["Model", "Mouse"], frameon=False)

    ax.set_ylim([-0.05, 1.05])

    ax.set_xlabel("Change magnitude")
    ax.set_ylabel("P(lick)")

    ax.spines["right"].set_visible(False)
    ax.spines["top"].set_visible(False)

    if savepath is not None:
        plt.savefig(savepath, dpi=300)

    if showfig is True:
        plt.show()


def test_loss_function(datapath, plot_example=True, savepath=None):

    params = [10.0, 0.5, 0.0]
    time_shift = 0

    signal_matrix, lick_matrix = create_vectorised_data(datapath)
    batched_predict_lick = vmap(predict_lick, in_axes=(None, 0))


    # Plot examples
    if plot_example is True:
        for s, l in zip(signal_matrix, lick_matrix):
            prediction = predict_lick(params, s)
            loss = matrix_cross_entropy_loss(lick_matrix=l, prediction_matrix=prediction)
            biased_loss = matrix_weighted_cross_entropy_loss(lick_matrix=l, prediction_matrix=prediction, alpha=20)

            fig, axs = plt.subplots(2, 1, figsize=(8, 8), sharex=True)
            axs[0].plot(s[l != 99])
            axs[1].plot(l[l != 99])
            axs[1].plot(prediction[l != 99])
            axs[1].text(x=0.1, y=0.8, s="Loss: " + str(loss), fontsize=12)
            axs[1].text(x=0.1, y=0.6, s="Biased loss: " + str(biased_loss), fontsize=12)
            axs[1].legend(["Mouse lick", "Model prediction"], frameon=False)

            axs[0].spines["top"].set_visible(False)
            axs[0].spines["right"].set_visible(False)
            axs[1].spines["top"].set_visible(False)
            axs[1].spines["right"].set_visible(False)

            axs[0].set_ylabel("Stimulus speed")
            axs[1].set_xlabel("Time (frames)")
            axs[1].set_ylabel("P(lick)")

            axs[1].set_ylim([0, 1])


            plt.show()
    # Plot loss across all examples
    all_zero_prediction_matrix = onp.zeros(shape=np.shape(lick_matrix))
    all_zero_prediction_matrix[:] = 1e-8

    all_one_prediction_matrix = onp.zeros(shape=np.shape(lick_matrix))
    all_one_prediction_matrix[:] = 1 - 1e-8

    alpha_list = np.arange(1, 20)

    all_zero_loss_store = list()
    all_one_loss_store = list()
    model_loss_store = list()

    prediction_matrix = batched_predict_lick(params, signal_matrix)

    for n, alpha in tqdm(enumerate(alpha_list)):
        all_zero_loss = matrix_weighted_cross_entropy_loss(lick_matrix, all_zero_prediction_matrix, alpha=alpha)
        model_loss = matrix_weighted_cross_entropy_loss(lick_matrix, prediction_matrix, alpha=alpha)

        all_zero_loss_store.append(all_zero_loss)
        model_loss_store.append(model_loss)

    fig, axs = plt.subplots(1, 1, figsize=(8, 8))
    axs.plot(alpha_list, all_zero_loss_store)
    axs.plot(alpha_list, model_loss_store)
    axs.legend(["All zero", "Model"], frameon=False)

    axs.set_ylabel("Cross entropy loss")
    axs.set_xlabel("False negative bias (alpha)")

    if savepath is not None:
        plt.savefig(savepath)
    plt.show()


def get_hazard_rate(hazard_rate_type="subjective", datapath=None, plot_hazard_rate=False, figsavepath=None,
                    constant_val=0.001):
    """
    The way I see it, there is 3 types of varying hazard rate.
    1. "normative": One specified by the experimental design;
    the actual distribution where the trial change-times are sampled from
    2. "experimental": The distribution in the trial change-times
    3. "subjective": The distribution experienced by the mice
    :param hazard_rate_type:
    :param datapath:
    :param plot_hazard_rate:
    :param figsavepath:
    :return:
    """
    with open(datapath, "rb") as handle:
        exp_data = pkl.load(handle)

    change_time = exp_data["change"].flatten()
    signal = exp_data["ys"].flatten()
    num_trial = len(change_time)
    signal_length_list = list()
    for s in signal:
        signal_length_list.append(len(s[0]))

    max_signal_length = max(signal_length_list)

    experimental_hazard_rate = onp.histogram(change_time, range=(0, max_signal_length), bins=max_signal_length)[0]
    experimental_hazard_rate = experimental_hazard_rate / num_trial

    if hazard_rate_type == "subjective":
        # get change times
        # remove change times where the mice did a FA, or a miss
        outcome = exp_data["outcome"].flatten()
        hit_index = onp.where(outcome == "Hit")[0]
        num_subjective_trial = len(hit_index)
        subjective_change_time = change_time[hit_index]
        subjective_hazard_rate = onp.histogram(subjective_change_time, range=(0, max_signal_length), bins=max_signal_length)[0]

        # convert from count to proportion (Divide by num_trial or num_subjective_trial?)
        subjective_hazard_rate = subjective_hazard_rate / num_subjective_trial

        assert len(subjective_hazard_rate) == max_signal_length

        hazard_rate_vec = subjective_hazard_rate

    elif hazard_rate_type == "experimental":
        hazard_rate_vec = experimental_hazard_rate
    elif hazard_rate_type == "normative":
        pass
    elif hazard_rate_type == "constant":
        hazard_rate_constant = constant_val
        hazard_rate_vec = np.repeat(hazard_rate_constant, max_signal_length)
    elif hazard_rate_type == "random":
        hazard_rate_vec = onp.random.normal(loc=0.0, scale=0.5, size=(max_signal_length, ))
        hazard_rate_vec = standard_sigmoid(hazard_rate_vec)
    if plot_hazard_rate is True:
        fig, ax = plt.subplots(1, 1, figsize=(8, 6))
        ax.plot(subjective_hazard_rate, label="Subjective hazard rate")
        ax.plot(experimental_hazard_rate, label="Experiment hazard rate")

        ax.set_xlabel("Time (frames)")
        ax.set_ylabel("P(change)")
        ax.legend(frameon=False)
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)

        if figsavepath is not None:
            plt.savefig(figsavepath, dpi=300)
        plt.show()

    # Make the transition matrix based on hazard_rate_vec
    transition_matrix_list = list()
    for hazard_rate in hazard_rate_vec:
        transition_matrix = np.array([[1 - hazard_rate, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0],
                                  [0, 1, 0, 0, 0, 0],
                                  [0, 0, 1, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 0],
                                  [0, 0, 0, 0, 1, 0],
                                  [0, 0, 0, 0, 0, 1]])
        transition_matrix_list.append(transition_matrix)

    return hazard_rate_vec, transition_matrix_list


def make_transition_matrix(hazard_rate_vec):
    # standard logistic function to contrain values to [0, 1]
    # hazard_rate_vec = standard_sigmoid(hazard_rate_vec)

    # Custom logistic function to constrain values
    small_value = 0.01
    hazard_rate_vec = nonstandard_sigmoid(hazard_rate_vec, min_val=0, max_val=1, k=1, midpoint=2)

    transition_matrix_list = list()
    for hazard_rate in hazard_rate_vec:
        transition_matrix = np.array([[1 - hazard_rate, hazard_rate/5.0, hazard_rate/5.0, hazard_rate/5.0,
                                       hazard_rate/5.0, hazard_rate/5.0],
                                  [0, 1, 0, 0, 0, 0],
                                  [0, 0, 1, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 0],
                                  [0, 0, 0, 0, 1, 0],
                                  [0, 0, 0, 0, 0, 1]])
        transition_matrix_list.append(transition_matrix)

    return transition_matrix_list


def gradient_descent_w_cv(exp_data_path, training_savepath, 
	                      init_param_vals=np.array([10.0, 0.5]),
                          time_shift_list=np.arange(0, 5), num_epoch=10, 
                          fit_hazard_rate=True, cv_random_seed=None, n_params=2, 
                          batch_size=None, fitted_params=None):
    """
    Runs gradient descent to optimise parameters of the HMM to fit behavioural results with cross validation.
    :param exp_data_path: path to file containing the experimental data
    :param training_savepath: path to save the training data results
    :param init_param_vals: initial parameter values
    :param time_shift_list: list of time shift to loop over
    :param num_epoch: number of epochs to run gradient descent
    :param fit_hazard_rate: if True, fits hazard rate parameters
    :param cv_random_seed: random seed used for test-validation-train split
    :param n_params: number of non-hazard-rate parameters to fit, subsequent parameters are hazard rate parameters
    :param batch_size: size of batch for minibatch gradient descent, if None, then the entire batch is used
    :param fitted_params: (optional) list containing the names of the listed parameters, useful for model comparison
    :return:
    """
    # TODO: remove the globals
    global num_non_hazard_rate_params
    num_non_hazard_rate_params = n_params

    # Define global variables used by loss_function
    global signal_matrix
    global lick_matrix

    signal_matrix, lick_matrix = create_vectorised_data(
        exp_data_path, lick_exponential_decay=False, decay_constant=1.0
    )
    # initialise hazard rate using experimental values
    # hazard_rate, _ = get_hazard_rate(hazard_rate_type="subjective", datapath=exp_data_path)

    # fit constant hazard rate
    hazard_rate = 0.01

    if fit_hazard_rate is True:
        init_param_vals = np.concatenate([init_param_vals, hazard_rate])

    global time_shift
    # time_shift = 0  # For debugging only

    # loss_val_matrix = onp.zeros((num_epoch, len(time_shift_list)))

    # define batched prediction function using vmap
    global batched_predict_lick

    if fit_hazard_rate is True:
        batched_predict_lick = vmap(predict_lick_w_hazard_rate, in_axes=(None, 0))
    else:
        batched_predict_lick = vmap(predict_lick, in_axes=(None, 0))
        global transition_matrix_list
        _, transition_matrix_list = get_hazard_rate(hazard_rate_type="subjective", datapath=exp_data_path)

    # for cases where the cumulative lick is used
    global batched_cumulative_lick
    batched_cumulative_lick = vmap(predict_cumulative_lick, in_axes=(None, 0))

    with open(exp_data_path, "rb") as handle:
        exp_data = pkl.load(handle)

    if cv_random_seed is None:
        cv_random_seed = onp.random.seed()

    mouse_reaction = exp_data["outcome"].flatten()

    le = LabelEncoder()
    mouse_reaction = le.fit_transform(mouse_reaction.tolist())

    mouse_reaction_df = pd.DataFrame({'trial': np.arange(0, len(mouse_reaction)),
                                      'outcome': mouse_reaction})
    y = mouse_reaction_df["outcome"]

    # y_train and y_test are just placeholders. This is only used to obtain the indices.
    X_dev, X_test, y_dev, y_test = train_test_split(
        mouse_reaction_df, y, test_size=0.1, random_state=cv_random_seed, stratify=y
    )

    # further split validation set
    X_train, X_val, y_train, y_val = train_test_split(
        X_dev, y_dev, test_size=0.1, random_state=cv_random_seed, stratify=y_dev
    )

    signal_matrix_test = signal_matrix[X_test["trial"], :]
    signal_matrix_val = signal_matrix[X_val["trial"], :]
    signal_matrix_train = signal_matrix[X_train["trial"], :]

    lick_matrix_test = lick_matrix[X_test["trial"], :]
    lick_matrix_val = lick_matrix[X_val["trial"], :]
    lick_matrix_train = lick_matrix[X_train["trial"], :]

    # For use when training a constant hazard rate
    global max_signal_length
    max_signal_length = np.shape(lick_matrix)[1]

    # Store model data
    param_list = list()  # list of list, each list is the parameter values for a particular time shift
    train_loss_list = list()
    val_loss_list = list()
    epoch_num_list = list()
    test_loss_list = list()
    time_shift_store = list()

    # Hyperparameters for training the model
    step_size = 0.02  # orignally 0.01
    opt_init, opt_update = optimizers.adam(step_size, b1=0.9, b2=0.999, eps=1e-8)

    for time_shift in tqdm(time_shift_list):
        print("Time shift: ", str(time_shift))

        # Minibatch gradient descent
        if batch_size is not None:
            num_train = onp.shape(signal_matrix_train)[0]
            num_complete_batches, leftover = divmod(num_train, batch_size)
            num_batches = num_complete_batches + bool(leftover)

            def data_stream():
                rng = npr.RandomState(0)
                while True:
                    perm = rng.permutation(num_train)
                    for i in range(num_batches):
                        batch_idx = perm[i * batch_size:(i + 1) * batch_size]
                        yield signal_matrix_train[batch_idx, :], lick_matrix_train[batch_idx, :]

            batches = data_stream()

        opt_state = opt_init(init_param_vals)

        @jit
        def step(i, opt_state, batch):
            params = optimizers.get_params(opt_state)
            g = grad(loss_function_batch)(params, batch)
            return opt_update(i, g, opt_state)

        for epoch in range(num_epoch):
            # print("Epoch: ", str(epoch))
            if batch_size is not None:
                for _ in range(num_batches):
                    opt_state = step(epoch, opt_state, next(batches))
                    # print("Parameters:", opt_state[0][0:6])
            else:
                opt_state = step(epoch, opt_state, (signal_matrix_train, lick_matrix_train))

            if epoch % 10 == 0:
                params = optimizers.get_params(opt_state)
                train_loss = loss_function_batch(params, (signal_matrix_train, lick_matrix_train))
                val_loss = loss_function_batch(params, (signal_matrix_val, lick_matrix_val))
                print("Training loss:", train_loss)
                train_loss_list.append(train_loss)

                print("Validation loss:", val_loss)
                val_loss_list.append(val_loss)

                param_list.append(params)
                epoch_num_list.append(epoch)

                time_shift_store.append(time_shift)

                # TODO: Write function to auto-stop based on CV loss

            # get test loss at the end of training
            if epoch == num_epoch:
                # TODO: used the best params...
                test_loss = loss_function_batch(params, (signal_matrix_test, lick_matrix_test))
                print("Test loss: " + str(test_loss))
                test_loss_list.append(test_loss)

    training_result = dict()
    training_result["train_loss"] = train_loss_list
    training_result["val_loss"] = val_loss_list
    training_result["test_loss"] = test_loss_list
    training_result["param_val"] = param_list
    training_result["init_param_val"] = init_param_vals
    training_result["time_shift"] = time_shift_store
    training_result["epoch"] = epoch_num_list
    training_result["batch_size"] = batch_size
    training_result["step_size"] = step_size
    training_result["train_set_size"] = len(y_train)
    training_result["val_set_size"] = len(y_val)
    training_result["test_set_size"] = len(y_test)

    training_result["mean_train_loss"] = np.array(train_loss_list) / len(y_train)
    training_result["mean_val_loss"] = np.array(val_loss_list) / len(y_val)
    training_result["mean_test_loss"] = np.array(test_loss_list) / len(y_test)

    if fitted_params is not None:
        training_result["fitted_params"] = fitted_params

    with open(training_savepath, "wb") as handle:
        pkl.dump(training_result, handle)


def standard_sigmoid(input):
    # Naive version:
    # output = 1.0 / (1.0 + np.exp(-input))
    # Numerically stable version
    """
    if input >= 0:
        z = np.exp(-input)
        output = 1.0 / (1.0 + z)
    else:
        z = np.exp(input)
        output = z / (1.0 + z)
    """

    # Numerically stable and applied to an array
    output = np.where(input >= 0,
                    1.0 / (1.0 + np.exp(-input)),
                    np.exp(input) / (1.0 + np.exp(input)))


    return output

def nonstandard_sigmoid(input, min_val=0.0, max_val=1.0, k=1, midpoint=0.5):
    # Numerically stable and applied to an array
    output = np.where(
        input >= 0,
        (max_val - min_val) / (1.0 + np.exp(-(input - midpoint) * k)) + min_val,
        (max_val - min_val) * np.exp((input - midpoint) * k) / (1.0 + np.exp((input - midpoint) * k))
        + min_val
    )
    return output

def softmax(input_vec):
    # native implementation
    output = np.exp(input_vec) / np.sum(np.exp(input_vec))
    return output


def inverse_nonstandard_sigmoid(input, min_val=0.0, max_val=1.0):
    # Numerically stable and applied to an array
    output = np.where(
        input >= 0,
        (max_val - min_val) / (1.0 + np.exp(-input)) + min_val,
        (max_val - min_val) * np.exp(input) / (1.0 + np.exp(input)) + min_val
    )
    return output


def predict_lick_w_hazard_rate(param_vals, signal):
    # NOTE: This function also depends on the global parameter "time_shift"

    # impose some hard boundaries
    global transition_matrix_list
    # transition_matrix_list = make_transition_matrix(hazard_rate_vec=param_vals[num_non_hazard_rate_params:])
    # ^ note this works due to zero-indexing. (if num=2, then we start from index 2, which is the 3rd param)

    # constant tunable hazard rate
    hazard_rate_param = np.repeat(param_vals[num_non_hazard_rate_params:], max_signal_length)
    transition_matrix_list = make_transition_matrix(hazard_rate_vec=hazard_rate_param)

    # multiply standard normal by constant: aX + b = N(au + b, a^2\sigma^2)
    posterior = forward_inference_custom_transition_matrix(signal)

    small_value = 1e-6
    posterior = np.where(posterior >= 1.0, 1-small_value, posterior) # I've seen
    # rounding errors where the output is 1.0000001 (strange)
    # Flagging just in case it as cosequences to other models...
    posterior = np.where(posterior <= 0.0, small_value, posterior)  # prevent underflow

    # no noise in the signal
    p_lick = apply_strategy(posterior, k=param_vals[0], midpoint=param_vals[1])

    # Add time shift
    if time_shift > 0:
        baseline = 0.88888888 # nan placeholder
        p_lick = np.concatenate([np.repeat(baseline, time_shift), p_lick])
        p_lick = p_lick[0:len(signal.flatten())]  # clip p(lick) to be same length as actual lick
    elif time_shift < 0:
        # backward shift
        baseline = 0.88888888  # nan placecholder
        p_lick = np.concatenate([p_lick, np.repeat(baseline, abs(time_shift))])
        p_lick = p_lick[abs(time_shift):(len(signal.flatten()) + abs(time_shift))]

    # hard threshold to make sure there are no invalid values due to overflow/underflow
    # small_value = 1e-9
    # p_lick = np.where(p_lick <=0.0, small_value, p_lick)
    # p_lick = np.where(p_lick >=1.0, 1-small_value, p_lick)

    return p_lick


def predict_cumulative_lick(param, p_lick_instant):
    p_lick_cumulative_list = list()
    p_no_lick_cumulative_list = list()
    p_no_lick_instant = 1 - p_lick_instant

    p_lick_cumulative_list.append(p_lick_instant[0])
    p_no_lick_cumulative_list.append(1 - p_lick_instant[0])

    for time_step in np.arange(1, np.shape(p_lick_instant)[0]):
        p_no_lick_cumulative_list.append(p_no_lick_cumulative_list[time_step-1] * (1 - p_lick_instant[time_step]))
        p_lick_cumulative = p_no_lick_cumulative_list[time_step-1] * p_lick_instant[time_step] + \
                            p_lick_cumulative_list[time_step-1]

        p_lick_cumulative_list.append(p_lick_cumulative)

    return np.array(p_lick_cumulative_list)



def predict_lick_control(param, signal):
    posterior = forward_inference_custom_transition_matrix(signal.flatten())
    p_lick = np.where(posterior > 1, 1, posterior) # I've seen rounding errors where the output is 1.0000001 (strange)
    # Flagging just in case it as cosequences to other models...
    return p_lick


def get_trained_hazard_rate(training_savepath, num_non_hazard_rate_param=2, epoch_num=1,
                            param_process_method="sigmoid"):
    with open(training_savepath, "rb") as handle:
        training_result = pkl.load(handle)

    trained_hazard_rate = training_result["param_val"][epoch_num][num_non_hazard_rate_param:]

    if param_process_method == "sigmoid":
        trained_hazard_rate = standard_sigmoid(trained_hazard_rate)

    return trained_hazard_rate


def benchmark_model(datapath, training_savepath, figsavepath=None, alpha=1):

    signal_matrix, lick_matrix = create_vectorised_data(datapath)
    # Dummy model
    all_zero_prediction_matrix = onp.zeros(shape=np.shape(lick_matrix))
    small_value = 0.01
    all_zero_prediction_matrix[:] = small_value

    with open(training_savepath, "rb") as handle:
        training_result = pkl.load(handle)

    loss = training_result["loss"][-1]
    all_zero_loss = matrix_weighted_cross_entropy_loss(
        lick_matrix, all_zero_prediction_matrix, alpha=alpha
    )

    figure, ax = plt.subplots(1, 1, figsize=(8, 6))

    ax.bar(x=[1, 2], height=[all_zero_loss, loss], tick_label=["All " + str(small_value), "Trained model"])

    ax.set_ylabel("Cross entropy loss")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    if figsavepath is not None:
        plt.savefig(figsavepath, dpi=300)

    plt.show()


def plot_time_shift_training_result(training_savepath, figsavepath=None, time_shift_list = np.arange(0, 22, 2),
                                    num_step=20, num_epoch_per_step=10):
    with open(training_savepath, "rb") as handle:
        training_result = pkl.load(handle)

    loss = training_result["loss"]
    final_losses = loss[num_step-1::num_step] # 9th, 19th, etc... (0 indexing)

    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    ax.plot(time_shift_list, final_losses)
    ax.scatter(time_shift_list, final_losses)
    ax.set_ylabel("Loss at %s-th iteration" % str(num_step * num_epoch_per_step))
    ax.set_xlabel("Time shift (frames)")

    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    if figsavepath is not None:
        plt.savefig(figsavepath, dpi=300)

    plt.show()


def gradient_clipping(gradients, type="L2_norm"):
    if type == "L2_norm":
        threshold = 10 # need to read what to set this to...
        new_gradients = gradients * threshold / (gradients ** 2 / len(gradients)) # TODO: should be l2 norm

    return new_gradients


def main(model_number=99, exp_data_number=83, run_test_on_data=False, 
         run_gradient_descent=False, run_plot_training_loss=False, 
         run_plot_sigmoid=False, run_plot_test_loss=False, run_model=False, 
         run_plot_time_shift_test=False, run_plot_hazard_rate=False, 
         run_plot_trained_hazard_rate=False, run_benchmark_model=False, 
         run_plot_time_shift_training_result=False, run_plot_posterior=False, 
         run_plot_signal=False, run_plot_trained_posterior=False, 
         run_plot_trained_sigmoid=False):
    home = expanduser("~")
    print("Running model: ", str(model_number))
    print("Using mouse: ", str(exp_data_number))

    # datapath = "/media/timothysit/180C-2DDD/second_rotation_project/exp_data/subsetted_data/data_IO_083.pkl"
    main_folder = os.path.join(
        home, "Dropbox/notes/Projects/second_rotation_project/normative_model"
    )
    # TODO: generalise the code below
    datapath = os.path.join(
        main_folder, 
        "exp_data/subsetted_data/data_IO_0" + str(exp_data_number) + ".pkl"
    )
    model_save_path = os.path.join(
        main_folder, "hmm_data/model_response_0" + str(exp_data_number) + "_"
                                   + str(model_number) + ".pkl"
    )
    fig_save_path = os.path.join(
        main_folder, "figures/model_response_0" + str(exp_data_number) + "_"
                                   + str(model_number) + ".png"
    )
    training_savepath = os.path.join(
        main_folder, "hmm_data/training_result_0" + str(exp_data_number) + "_"
                                   + str(model_number) + ".pkl"
    )

    print("Saving training data to: ", training_savepath)
    print("Mouse data path: ", datapath)

    """
    param: 
    1. k parameter in the sigmoid function
    2. false_negative value in cost-benefit function
    3. midpoint of the sigmoid decision function
    """
    if run_test_on_data is True:
        test_on_data(change_val=1.25, exp_data_file=datapath)

    if run_model is True:
        run_through_dataset_fit_vector(
            datapath=datapath, savepath=model_save_path, 
            training_savepath=training_savepath, num_non_hazard_rate_param=0, 
            fit_hazard_rate=True, cv=True, param=None
        )

    if run_gradient_descent is True:
        gradient_descent_w_cv(
            exp_data_path=datapath, training_savepath=training_savepath,
            init_param_vals=np.array([10.0, 0.5]), n_params=2, 
            fit_hazard_rate=True, time_shift_list=np.arange(0, 11), 
            num_epoch=500, batch_size=512,
            fitted_params=["sigmoid_k", "sigmoid_midpoint", "hazard_rate"]
        )

    if run_plot_test_loss is True:
        figsavepath = os.path.join(
            home, 
            "Dropbox/notes/Projects/second_rotation_project/normative_model/figures/test_alpha_model_"
                + str(model_number) + ".png"
        )
        test_loss_function(datapath, plot_example=False, savepath=figsavepath)

    if run_plot_training_loss is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/training_result_model" + str(model_number)
        )
        nmt_plot.plot_training_loss(
            training_savepath, figsavepath=figsavepath, cv=True, time_shift=False
        )

    if run_plot_sigmoid is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/transfer_function_model" + str(model_number)
        )
        nmt_plot.plot_sigmoid_comparisions(
            training_savepath, plot_least_loss_sigmoid=True, 
            figsavepath=figsavepath
        )

    if run_plot_trained_sigmoid is True:
        exp_data_number_list = [75, 78, 79, 80, 81, 83]
        label_list = [1, 2, 3, 4, 5, 6]
        figsavepath = os.path.join(
            main_folder, "figures", "sigmoid",
            "mouse_sigmoid_comparison_model_" + str(model_number)
        )
        plt.style.use(
            "~/Dropbox/notes/Projects/second_rotation_project/normative_model/ts.mplstyle"
        )
        fig, ax = plt.subplots(figsize=(4, 4))
        sigmoid_func = functools.partial(
            apply_strategy, max_val=1.0, min_val=0.0, policy="sigmoid", 
            epsilon=0.1, lick_bias=0.5
        )
        for exp_data_number, label in zip(exp_data_number_list, label_list):
            training_savepath = os.path.join(
                main_folder, 
                "hmm_data/training_result_0" + str(exp_data_number) + "_"
                    + str(model_number) + ".pkl"
            )
            with open(training_savepath, "rb") as handle:
                training_result = pkl.load(handle)
            fig, ax = nmt_plot.plot_trained_sigmoid(
                fig, ax, training_result, sigmoid_func, training_epoch=None, 
                label=label
            )

        ax.grid()
        ax.legend(title="Mouse")
        fig.savefig(figsavepath)

        plt.show()

    if run_plot_trained_hazard_rate is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/trained_hazard_rate_model_" + str(model_number) + "_mouse_"
                + str(exp_data_number)
        )
        sigmoid_function = functools.partial(
            nonstandard_sigmoid, min_val=0, max_val=1.0, k=1, midpoint=0.5
        )
        fig, ax = nmt_plot.plot_trained_hazard_rate(
            training_savepath, sigmoid_function, num_non_hazard_rate_param=0
        )
        fig.set_size_inches(8, 4)
        fig.savefig(figsavepath)

    if run_plot_time_shift_test is True:
        for trial_num in np.arange(0, 5):
            nmt_plot.plot_time_shift_test(
                datapath, param=[10, 0.5], time_shift_list=[0, -10], 
                trial_num=trial_num
            )

    if run_plot_hazard_rate is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/hazard_rate_subjective_mouse_" + str(exp_data_number) + ".png"
        )

        mouse_hazard_rate, _ = get_hazard_rate(
            hazard_rate_type="subjective", datapath=datapath, 
            plot_hazard_rate=False, figsavepath=None
        )

        model_hazard_rate = get_trained_hazard_rate(
            training_savepath, num_non_hazard_rate_param=6, epoch_num=348, 
            param_process_method="sigmoid"
        )
        model_hazard_rate = model_hazard_rate[6:-11]  # remove the time-shifted bits at the end
        plt.plot(model_hazard_rate)
        fig = nmt_plot.compare_model_mouse_hazard_rate(
            model_hazard_rate, mouse_hazard_rate, scale_method="sum"
        )
        plt.show()

    if run_benchmark_model is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/loss_benchmark_model_" + str(model_number) + ".png"
        )
        benchmark_model(
            datapath, training_savepath, figsavepath=figsavepath, alpha=1
        )

    if run_plot_time_shift_training_result is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/time_shift_loss_model_" + str(model_number) + ".png"
        )
        plot_time_shift_training_result(
            training_savepath=training_savepath, figsavepath=figsavepath,
            time_shift_list=np.arange(0, 10, 1), num_step=40, 
            num_epoch_per_step=10
        )

    if run_plot_posterior is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/pure_posterior_exp_transition_matrix_window_40.png"
        )
        nmt_plot.plot_posterior(datapath=datapath, figsavepath=None)

    if run_plot_signal is True:
        nmt_plot.plot_signals(datapath)

    if run_plot_trained_posterior is True:
        figsavepath = os.path.join(
            main_folder, 
            "figures/trained_posterior_realtime" + "_model_" +str(model_number) +
                "_mouse_" + str(exp_data_number)
        )
        nmt_plot.plot_trained_posterior(
            datapath, training_savepath, plot_peri_stimulus=False,
            num_examples=10, random_seed=777, plot_cumulative=False,
            figsavepath=figsavepath
        )

        figsavepath = os.path.join(
            main_folder, 
            "figures/trained_posterior_peri_stimulus_time" + "_model_" +
                str(model_number) + "_mouse_" + str(exp_data_number)
        )
        nmt_plot.plot_trained_posterior(
            datapath, training_savepath, plot_peri_stimulus=True,
            num_examples=10, random_seed=777, figsavepath=figsavepath, 
            plot_cumulative=False
        )


if __name__ == "__main__":
    exp_data_number_list = [75, 78]  # [75, 78, 79, 80, 81, 83]
    for exp_data_number in exp_data_number_list:
        main(model_number=68, exp_data_number=exp_data_number, run_test_on_data=False, run_gradient_descent=True,
             run_plot_training_loss=False, run_plot_sigmoid=False,
             run_plot_test_loss=False, run_model=False, run_plot_time_shift_test=False,
             run_plot_hazard_rate=False, run_plot_trained_hazard_rate=False, run_benchmark_model=False,
             run_plot_time_shift_training_result=False, run_plot_posterior=False,
             run_plot_signal=False, run_plot_trained_posterior=False, run_plot_trained_sigmoid=False)

